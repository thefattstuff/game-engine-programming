using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class canBeDamaged : MonoBehaviour
{

    [SerializeField, Range(1f, 200f)]
    public float health = 100f;
    // Stats for health
    [SerializeField] private float maxHealth = 100f;
    [SerializeField, Range(1f, 100f)]
    private float startHealth = 100f;

    public en downEnemy;
    public float enemyMath;

    public Animator anim;


    private float lifespan = 1.50f;

    public GameObject WeaponContainer;
    public GameObject Canvas;

    public bool test = false;

    public GameObject[] itemDrops;
    public GameObject drop;

    public Transform here;

    public playerController player;

    public GameManager gameManager;

    // Use this for initialization
    void Start()
    {
        test = false;
        anim = GetComponentInParent<Animator>();

        drop = (itemDrops[Random.Range(0, itemDrops.Length)]);

       
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            
            animOff();
            Destroy(Canvas);
            Destroy(WeaponContainer);
            Destroy(this.gameObject, lifespan);

            
        }
        
        
    

}


    public void animOff()
    {
        Destroy(anim);

        int i;
        Rigidbody[] childRBs = GetComponentsInChildren<Rigidbody>();
        for (i = 0; i < childRBs.Length; i++)
        {
            childRBs[i].isKinematic = false;
        }

        Collider[] childColliders = GetComponentsInChildren<Collider>();
        for (i = 0; i < childColliders.Length; i++)
        {
            childColliders[i].enabled = true;
        }



        if (test == false)
        {

            test = true;
            droper();
            GameObject g = this.gameObject;
            en downEnemy = g.GetComponentInParent<en>();
            enemyMath = downEnemy.currentActiveEnemies;
            enemyMath--;
            downEnemy.currentActiveEnemies = enemyMath;

            gameManager.enemiesLeft--;
            Debug.Log(gameManager.enemiesLeft);
        }
    }


    public void animOn()
    {

        int i;
        Rigidbody[] childRBs = GetComponentsInChildren<Rigidbody>();
        for (i = 0; i < childRBs.Length; i++)
        {
            childRBs[i].isKinematic = true;
        }



    }

    public void droper()
    {
        drop = Instantiate(drop, here.position, Quaternion.identity) as GameObject;
        
    }
}
