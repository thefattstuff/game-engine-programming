using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class OptionsManager : MonoBehaviour {
    // for sound
    public Slider volume;
    public AudioSource sound;

    // for quality
    public ToggleGroup toggleGroup;
    private int quality;

    public GameObject optionsUI;
    public GameObject MainUI;

    void Awake()
    {
        // loads player Pref Volume
        volume.value = PlayerPrefs.GetFloat("volume");
    }
    // Use this for initialization
    void Start()
    {
        // hide options menu
        optionsUI.SetActive(false);

        // setting quality to Player prefs
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("quality"));
        // get quality level
        quality = QualitySettings.GetQualityLevel();

        

        // loads player Pref Volume
        volume.value = PlayerPrefs.GetFloat("volume");

        if (quality == 0)
        {
            toggleGroup.transform.Find("Toggle: Low").gameObject.GetComponent<Toggle>().isOn = true;
        }
        else if (quality == 3)
        {
            toggleGroup.transform.Find("Toggle: Medium").gameObject.GetComponent<Toggle>().isOn = true;
        }
        else if (quality == 5)
        {
            toggleGroup.transform.Find("Toggle: High").gameObject.GetComponent<Toggle>().isOn = true;
        }
        
    }
	
	// Update is called once per frame
	void Update () {
        // Sets volume from slider
        sound.volume = volume.value;
        
	}


    // resolution settings

    public void lowRez()
    {
        QualitySettings.SetQualityLevel(0);
    }

    public void medRez()
    {
        QualitySettings.SetQualityLevel(3);
    }

    public void highRez()
    {
        QualitySettings.SetQualityLevel(5);
    }

    // for saving player prefs

    public void saveButton()
    {
        // sets player pref for volume
        PlayerPrefs.SetFloat("volume", volume.value);
        // saves resolution setting
        PlayerPrefs.SetFloat("quality", QualitySettings.GetQualityLevel());
    }

    public void menuButton()
    {
       optionsUI.SetActive(false);
        MainUI.SetActive(true);
    }

    public void optionsButton()
    {
        optionsUI.SetActive(true);
        MainUI.SetActive(false);

    }
}
