using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class en : MonoBehaviour {
    public GameObject pickupPrefab;
    public float spawnDelay;
    private float nextSpawnTime = 1;
    private Transform tf;
    public GameObject spawnedPickup;
    public Vector3 scale;


    public float maxActiveEnemies = 5;

    public float currentActiveEnemies = 0;


    void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {

        if (maxActiveEnemies >= currentActiveEnemies)
        {
           
            // And it is time to spawn
            if (Time.time > nextSpawnTime)
            {
               
                // Spawn it and set the next time
                spawnedPickup = Instantiate(pickupPrefab, tf.position, Quaternion.identity) as GameObject;
                spawnedPickup.transform.SetParent(tf);
                nextSpawnTime = Time.time + spawnDelay;
                currentActiveEnemies++;
               

            }
        }
        else
        {
            
            // Otherwise, the object still exists, so postpone the spawn
            nextSpawnTime = Time.time + spawnDelay;
        }
    }

   

    private void OnDrawGizmos()
    {
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.color = Color.Lerp(Color.cyan, Color.clear, 0.5f);
        Gizmos.DrawCube(Vector3.up * scale.y / 2f, scale);
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(Vector3.zero, Vector3.forward * 0.4f);
    }
}
