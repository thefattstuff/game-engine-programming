using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class bullet : MonoBehaviour {
    
    private float lifespan = 5.00f;
    public Rigidbody body;

    
    public float damage;

    private float calc;
    private float newHealth;
    

    // Use this for initialization
    void Start () {
        decay();
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    // When Tigger check if object had the playerControler script
    private void OnTriggerEnter(Collider collider)
    {
        
        canBeDamaged health = GetComponent<canBeDamaged>();
        var script = collider.GetComponent<canBeDamaged>();
        
        // if object has player controller
        if (script != null)
        {
           
            GameObject g = collider.gameObject;
            GameObject b = GameObject.FindGameObjectWithTag("Equipped");
            health = g.GetComponent<canBeDamaged>();
            weaponPickUp weaponDamage = b.GetComponent<weaponPickUp>();
            damage = weaponDamage.damage;

            calc = health.health;
            newHealth = calc - damage;
            health.health = newHealth;

            Destroy(this.gameObject);
            

        }


    }

    public void decay()
    {
        Destroy(gameObject, lifespan);
    }
    
}
