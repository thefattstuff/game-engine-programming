using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthPickUps : PickUps {

    [SerializeField, Range(1f, 100f)]
    private float healAmount;
    public canBeDamaged health;
    private float healthCalc = 0f;
    public GameObject player;
   

	// Use this for initialization
	void Start () {
        health = GetComponent<canBeDamaged>();
	}
	
	// Update is called once per frame
	void Update () {

       
		
	}

    


    // calls heal function and pick up after collision 
    protected override void OnPickUp(playerController player)
    {
        Debug.LogFormat("I've been picked up by {0}!", player);
        base.OnPickUp(player);
        healPlayer();
    }

    // heals players by heal amount

    public void healPlayer()
    {
        
        healthCalc = player.GetComponent<canBeDamaged>().health;
        healthCalc = healthCalc + healAmount;
        Debug.Log(healthCalc);
        player.GetComponent<canBeDamaged>().health = healthCalc;


    }
}