using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class TestHealthScript : MonoBehaviour
{// for player health display
    public canBeDamaged health;
    public float displayHealth;
    public GameObject player;
    // for player health display
    public Text text;
    public Image filler; 
    
    // for player weapon display
    private weaponPickUp equippedWeapon;
    private Sprite weaponSprite;
    private GameObject weapon;
    public Image weaponImage;
    public Transform parent;

    public CanvasGroup can;

    private bool noWeapon = true;

  



    private void Awake()
    {
        text = GetComponent<Text>();
        health = GetComponent<canBeDamaged>();
        


    }

    private void Update()
    {
        // for player health
        player = GameObject.FindGameObjectWithTag("Player");
        health = GetComponent<canBeDamaged>();
        displayHealth = player.GetComponent<canBeDamaged>().health;
        filler.fillAmount = Mathf.Lerp(filler.fillAmount, displayHealth/100, Time.deltaTime);

       

        if (displayHealth < 0)
        {
            displayHealth = 0;
        }
        text.text = string.Format("Health: {0}%", displayHealth);
        // End player health

        //player weapon
        weapon = GameObject.FindGameObjectWithTag("Equipped");


        if (weapon != null)
        {
            weaponSprite = weapon.GetComponent<weaponPickUp>().weaponImage;
            weaponImage.transform.SetParent(parent);
            weaponImage.overrideSprite = weaponSprite;

            if (noWeapon == true)
            {
                can.alpha = 1f;
                noWeapon = false;
            }

        }

    }
    
}
