using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class playerController : MonoBehaviour {



    public Animator anim;
    private Rigidbody rb;
    [Header("Character Settings")]
    [SerializeField, Range(0f, 1f), Tooltip("Speed that the player moves at ")]
    private float speed = 0;

    

    public canBeDamaged Health { get; private set; }

    public bool test = false;
    private weaponPickUp weapon;
    private weaponPickUp trigger;

    
    


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        Health = GetComponent<canBeDamaged>();

        
    }
	
	// Update is called once per frame
	void Update () {
        // SETTING ANIMATION AND MOVMENTS
        anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));

        anim.SetFloat("Vertical", Input.GetAxis("Vertical") - 0.1f);

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed * Time.deltaTime);

       
        
        if (Input.GetMouseButtonDown(0) && test == true)
        {
            GameObject t = GameObject.FindGameObjectWithTag("Equipped");
            trigger = t.GetComponent<weaponPickUp>();
            trigger.pullTrigger();
            

        }


    }

    // IK ANIMATION BEGIN

    protected virtual void OnAnimatorIK()
    {
        
        

        if (test == false)
        {
            return;
        }
           
        
        else
        {
            
            if (test == true)
            {
                GameObject g = GameObject.FindGameObjectWithTag("Equipped");
                weapon = g.GetComponent<weaponPickUp>();

                
                anim.SetIKPosition(AvatarIKGoal.RightHand, weapon.equippedWeapon.RightHandIKTarget.position);
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
                anim.SetIKRotation(AvatarIKGoal.RightHand, weapon.equippedWeapon.RightHandIKTarget.rotation);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);

                anim.SetIKPosition(AvatarIKGoal.LeftHand, weapon.equippedWeapon.LeftHandIKTarget.position);
                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, weapon.equippedWeapon.LeftHandIKTarget.rotation);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
            }
        }
        
        
        
    }

    



}
