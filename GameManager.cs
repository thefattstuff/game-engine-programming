using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; private set; }
    //for gizmos
    public Vector3 scale;

    // So Gamemanger can look fo active player
    public GameObject activePlayer;

    //Instance of new player
    private GameObject Player;
   

    // Prefab of player
    public GameObject playerPrefab;
   

    // spawnpoint
    public Transform playerSpawnPoint;

    public float Lives = 3;

    //  For Menus
    public GameObject PauseUI;
    public static bool isPaused = false;

    public Transform attachmentPoint;


    // Game Music
    public AudioSource music;
    private float volume;

    //spawn audio
    public AudioClip spawn;
    public AudioSource audioSource;


    // endgame
    public int enemiesLeft = 25;


    private void Awake()
    {
        PauseUI.SetActive(false);
        // Set GameManager Instance to this game object
        Instance = this;
        Time.timeScale = 1f;
        // sets game volume to main volume
        volume = PlayerPrefs.GetFloat("volume");
        music.volume = volume;

    }

	// Use this for initialization
	void Start () {
        
    }

    // Update is called once per frame
    void Update()
    {
        activePlayer = GameObject.FindGameObjectWithTag("Player");

        if(enemiesLeft <=0)
        {
            SceneManager.LoadScene("main_menu");
        }

        if (activePlayer == null)
        {
            

            if (0 < Lives)
            {
                SpawnPlayer();
                
            }
            else
            {
                //load scene
                SceneManager.LoadScene("main_menu");
            }
        }

        //for pause 
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused == true)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

        



    //draws spawnpoint 
    private void OnDrawGizmos()
    {
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.color = Color.Lerp(Color.cyan, Color.clear, 0.5f);
        Gizmos.DrawCube(Vector3.up * scale.y / 2f, scale);
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(Vector3.zero, Vector3.forward * 0.4f);
    }

   // create new player
    private void SpawnPlayer()
    {
        audioSource.PlayOneShot(spawn, 0.5F);
        Player = Instantiate(playerPrefab, playerSpawnPoint.position, playerSpawnPoint.rotation) as GameObject;
        Lives--;
        Debug.Log(Lives);

    }


    public void Resume()
    {
        PauseUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    public void Pause()
    {
        PauseUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
}